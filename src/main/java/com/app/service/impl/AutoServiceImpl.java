package com.app.service.impl;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Auto;
import com.app.repository.AutoRepository;
import com.app.service.AutoService;


@Service
public class AutoServiceImpl implements AutoService {
	
	@Autowired
    private AutoRepository autoRepository;
	
	@Override
	public List<Auto> getListAutos() {
		return autoRepository.findAll();
	}

	@Override
	public Auto saveAuto(Auto auto) {
		return autoRepository.save(auto);
	}

}
