package com.app.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import com.app.model.Auto;

public interface AutoService {

	
	List<Auto> getListAutos();
	
	Auto saveAuto(Auto auto) throws Exception ;
}
