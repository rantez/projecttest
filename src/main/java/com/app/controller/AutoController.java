package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ResponseDTO;
import com.app.model.Auto;
import com.app.service.impl.AutoServiceImpl;

@RestController
public class AutoController {

   @Autowired
   private AutoServiceImpl autoService;
	
	@GetMapping("/listAutos")
	public ResponseEntity<ResponseDTO> getListAutos(){
	List<Auto> listAutor = autoService.getListAutos();
	if(!listAutor.isEmpty()) {

		return ResponseEntity.ok(new ResponseDTO(listAutor));
	}
		return ResponseEntity.ok(new ResponseDTO(111L,"Lista vacia","No se encontraron autos"));
	}
	
	@PostMapping("/addAuto")
	public ResponseEntity<ResponseDTO> addAuto(@RequestBody Auto auto){
		Auto result = autoService.saveAuto(auto);
		
		return ResponseEntity.ok(new ResponseDTO(result));
	}


}
