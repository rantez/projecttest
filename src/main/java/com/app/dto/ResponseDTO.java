package com.app.dto;



import java.io.Serializable;

public class ResponseDTO implements Serializable{

    private String mensajeError;
    private Long codError;
    private String mensajeErrorCliente;
    private Object responseData;

    public ResponseDTO(){}

    public ResponseDTO(Object responseData) {
        this.responseData = responseData;
    }

    public ResponseDTO(Long codError, String mensajeError, String mensajeErrorCliente) {
        this.mensajeError = mensajeError;
        this.codError = codError;
        this.mensajeErrorCliente = mensajeErrorCliente;
    }

    public Boolean huboError(){
        return getCodError() != null && getMensajeError() != null && getMensajeErrorCliente() != null;
    }

    public Long getCodError() {
        return codError;
    }

    public void setCodError(Long codError) {
        this.codError = codError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public String getMensajeErrorCliente() {
        return mensajeErrorCliente;
    }

    public void setMensajeErrorCliente(String mensajeErrorCliente) {
        this.mensajeErrorCliente = mensajeErrorCliente;
    }

    public Object getResponseData() {
        return responseData;
    }

    public void setResponseData(Object responseData) {
        this.responseData = responseData;
    }

}
