package com.app;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.app.dto.ResponseDTO;
import com.app.model.Auto;
import com.app.service.impl.AutoServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@SpringBootTest
@AutoConfigureMockMvc
class ProjectTestApplicationTests {

	@Autowired
	AutoServiceImpl autoService;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	Gson gson;
	
	@Autowired
	MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.standaloneSetup(ProjectTestApplicationTests.class).build();
	}

	@Test
	public void testListAutos() {
		assertNotNull(autoService.getListAutos());
		assertEquals(4, autoService.getListAutos().size());
	}

	@Test
	public void testListAutosSize() {
		assertEquals(4, autoService.getListAutos().size());
	}

	@Test
	public void testHappyPath() throws Exception {

		mvc.perform(get("/listAutos").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	@Test
	public void testPastObject() {
		Auto auto=new Auto();
		auto.setAnio(2002);
		auto.setColor("rojo");
		auto.setMarca("Ford");
		auto.setPrecio(1999);
				try {
					ResultActions resultActions = mvc.perform(post("/addAuto")
					.contentType(MediaType.APPLICATION_JSON)
					.content(objectMapper.writeValueAsString(auto)))
					.andExpect(status().isOk());
					MvcResult result = resultActions.andReturn();
					String contentAsString = result.getResponse().getContentAsString();
					ResponseDTO responser = gson.fromJson(contentAsString, ResponseDTO.class);
					assertNull(responser.getCodError());

				} catch (JsonProcessingException e) {
					e.printStackTrace();
				} catch (Exception e) {
					System.out.println("epa");
					e.printStackTrace();
				}

	
	}

}
